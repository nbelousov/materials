\input{../../../../main.tex}

\begin{document}

\setphysstyle{ЛФШ 2016}{Математика}{10 класс}

\parindent=5mm

\section{Производные}

\subsection{Мгновенная скорость}

Рассмотрим свободно падающее тело. Зависимость расстояния от времени описывается следующей формулой:

\[
S(t) = \dfrac{g t^2}{2}
\]

Рассмотрим промежуток времени $\Delta t$ начиная с $t_0$. При этом пройденный путь изменился с величины
$S_0$ до $S_0 + \Delta S$. Тогда величину $\Delta S$ можно найти следующим образом: 

\[
\Delta S = g t_0 \Delta t + \frac{g \Delta t^2}{2}
\]

Найдем среднюю скорость в зависимости от времени:

\[
v_\text{ср} = \dfrac{\Delta S}{\Delta t} = g t_0 + \frac{g \Delta t}{2}
\]

Хотим найти среднюю скорость за маленький промежуток времени, т.е. мгновенную скорость. По определению
мгновенная скорость равна: 

\[
v_\text{мгн} = \lim\limits_{\Delta t \rightarrow 0}\dfrac{\Delta S}{\Delta t} = \lim\limits_{\Delta t
    \rightarrow 0}\left( g t_0 + \frac{g \Delta t}{2} \right) = g t_0 
\]

На самом деле, здесь мы впервые столкнулись с производной.

\subsection{Секущая касательная}
\begin{wrapfigure}[14]{r}{5cm}
\begin{centering}
\vspace{-30pt}
\begin{tikzpicture}[domain=0:5.2, scale=.8]
    \pgfmathsetmacro{\M}{1};
    \pgfmathsetmacro{\N}{3};
    \pgfmathsetmacro{\a}{1.5};
%Сетка
    \draw[very thin,color=gray] (-1.1,-1.1) grid (5.9,5.9);
%Оси
    \draw[->] (-1.2,0) --++ (7.4,0) node[right] {$t \text{, с}$};
    \draw[->] (0,-1.2) --++ (0,7.4) node[above] {$s \text{, м}$};
%Функция
    \draw[thick] plot[id=x] function{-(x-3)*(x-2)/2+3.75} node[right] {$s(t)$};
%Подписи
    \draw (\M, .2) --++ (0, -.4) node[below] {$t_0$};
%Секущая
    \draw[blue] (\M, {-(\M-3)*(\M-2)/2+3.75}) -- (\N, {-(\N-3)*(\N-2)/2+3.75}) -- (\N,
    {-(\M-3)*(\M-2)/2+3.75}) node[midway, left] {$\Delta S$} --++({\M-\N}, 0) node[midway, below]
    {$\Delta t$}; 
%Угол \alpha
    \draw[blue] (2, {-(\M-3)*(\M-2)/2+3.75}) arc(0:{atan( (-(\N-3)*(\N-2) + (\M-3)*(\M-2))/4 )}:1) node[midway, left] {$\alpha$};
    \filldraw [blue] (\M, {-(\M-3)*(\M-2)/2+3.75}) circle(2pt) ;%node[below = 7pt, right = 1pt] {$M$};
    \filldraw [blue] (\N, {-(\N-3)*(\N-2)/2+3.75})  circle(2pt) ;%node[above] {$N$};
%Касательная
    \draw[thick, red] plot[domain = -1: 1.8] function{\a*x - \a*\M -(\M-3)*(\M-2)/2+3.75} node[above]{$f(t)$};
%Угол \beta
    \draw[red] ({\a*\M +(\M-3)*(\M-2)/2-3.75+1.4}, 0) arc(0:{atan(1.5)}:1) node[midway, left] {$\beta$};
\end{tikzpicture}
\end{centering}
\end{wrapfigure}

Давайте поймем, как это все выглядит графически. Рассмотрим произвольный график движения тела. Выберем
момент времени $t_0$ и будем смотреть, чему равна средняя скорость тела в произвольные интервалы времени
от $t_0$ до $t$. 

Т.к средняя скорость равна $v_\text{ср} = \frac{\Delta S}{\Delta t}$, то по графику видно, что средней
скорости соответствует тангенс наклона секущей к оси абсцисс (см. рисунок). 

\[
v_\text{ср} = \tan\alpha
\]

Теперь разберемся с мгновенной скоростью. По определению мгновенная скорость -- это средняя скорость тела
за бесконечно малый промежуток времени, т.е. в нашем случае при $t\rightarrow t_0$.

Что же произойдет с секущей, если мы устремим $t \rightarrow t_0$? Все просто: мы получим касательную к
графику функции $s(t)$ в точке $t_0$. Тогда, по аналогии с $v_\text{ср}$: 

\[
v_\text{мгн} = \tan\beta
\]

В математике предел отношения приращения функции к приращению аргумента (если он существует и конечен)
называют производной в данной точке. Записывается это следующим образом: 

\[
f'(x) = \dv{f}{x} = \lim\limits_{\Delta x\rightarrow 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}
\]

\begin{remark}
    Под записью $dx$ можно понимать бесконечно маленькую величину. Эту запись удобно использовать в
    решении задач.

    Взятие производной называется дифференцированием функции.

    По знаку производной можно судить о росте и убывании функции.
\end{remark}


\subsection{Где же еще встречаются производные в физике}

Вот где вы уже встречали производные:


\begin{enumerate}
    \item $\mathbf{v}  = \dv{\mathbf{r}}{t} = \mathbf{r'}$ -- скорость
    \item $\mathbf{a} = \dv{\mathbf{v}}{t} = \dv[2]{\mathbf{r}}{t} = \mathbf{v'} = \mathbf{r''}$ -- ускорение
    \item $\omega = \dv{\alpha}{t} = \alpha'$ -- угловая скорость
    \item $I = \dv{q}{t} = q'$~--- сила тока
    \item $N = \dv{A}{t} = A'$~--- мощность
    \item $F = \dv{p}{t}$~--- Второй закон ньютона в импульсной форме
\end{enumerate}

А еще на электростатике вы скоро встретитесь со следующим выражением для напряженности центрального
электрического поля через потенциал: 

\[
\mathbf{E} = - \dv{\varphi}{r}\cdot \dfrac{\mathbf{r}}{r}
\]

\subsection{Производные простых функций}
Получим выражения для производных для нескольких простых функций

\begin{example}
    Найти производную функции $f(x) = C$.
\end{example}



\[
f'(x) = \lim\limits_{\Delta x \to 0} \frac{\Delta f(x)}{\Delta x} = \lim\limits_{\Delta x \to 0} (C - C)
= 0
\]

Получили: $\boxed{C' = 0}$


\begin{example}
    Найти производную функции $f(x) = a x + b$.
\end{example}


\[
f'(x) = \lim\limits_{\Delta x \to 0} \frac{\Delta f(x)}{\Delta x} = \lim\limits_{\Delta x \to 0} \dfrac{a (x +\Delta x) + b - a x - b}{\Delta x} =
\lim\limits_{\Delta x \to 0} \dfrac{a \Delta x }{\Delta x} = a 
\]

Получили: $\boxed{\left(ax + b\right)' = a}$


\begin{example}
    Найти производную функции $f(x) = a x^2 + bx + c$.
\end{example}


Найдем $\Delta f(x)$:

\[
\begin{aligned}
\Delta f(x) &= a (x + \Delta x)^2 + b(x + \Delta x) + c - a x^2 - b x - c =\\
&=a x^2 + 2ax\Delta x + a \Delta x^2 + bx + b\Delta x - a x^2 - bx =\\
&= 2 a x \Delta x + a \Delta x ^2 + b \Delta x = \Delta x \left(2 a x + b + a \Delta x\right)
\end{aligned}
\]

Теперь найдем производную по определению:
\[
f'(x) = \lim\limits_{\Delta x \to 0} \frac{\Delta f(x)}{\Delta x} = \lim\limits_{\Delta x \to 0} \dfrac{
    \Delta x \left(2 a x + b + a \Delta x\right) }{\Delta x} = \lim\limits_{\Delta x \to 0} x \left( 2 a
  x + b + a \Delta x \right) = 2ax + b
\]

Получили: $\boxed{\left(ax^2 + b x + c\right)' = 2a x + b}$

\begin{proposition}
    $\lim\limits_{\Delta x \to 0} \dfrac{\sin \Delta x}{\Delta x} = 1$~--- Замечательный предел
\end{proposition}


Благодаря утверждению можем найти производные $\sin x$, $\cos x$

\begin{example}
    Найти производную $f(x) = \sin x$
\end{example}

Рассмотрим изменение $\Delta f(x)$:

\[
\Delta f(x) = \sin (x+ \Delta x) - \sin x = 2 \cdot \sin\left(\frac{x+\Delta x - x}{2}\right) \cdot
\cos\left(\frac{x+\Delta x + x}{2}\right) = 2 \cdot \sin\left(\frac{\Delta x}{2}\right) \cdot \cos\left(x
  + \frac{\Delta x}{2}\right) 
\]

Тогда производная равна:

\[
f'(x) =  \lim\limits_{\Delta x \to 0} \dfrac{2 \cdot \sin\left(\frac{\Delta x}{2}\right) \cdot \cos\left(x + \frac{\Delta
          x}{2}\right) }{\Delta x} = \lim\limits_{\Delta x \to 0} \left(\dfrac{\sin\left(\frac{\Delta
            x}{2}\right)}{\frac{\Delta x}{2}} \cdot \cos\left(x + \frac{\Delta x}{2}\right) \right) =
\cos x 
\]

Получили: $\boxed{\left(\sin x\right)' = \cos x}$

\begin{example}
    Найти производную $f(x) = \cos x$
\end{example}


Рассмотрим изменение $\Delta f(x)$:


\[
\Delta f(x) = \cos (x+ \Delta x) - \cos x = - 2 \cdot \sin\left(\frac{x+\Delta x - x}{2}\right) \cdot
\sin\left(\frac{x+\Delta x + x}{2}\right) = - 2 \cdot \sin\left(\frac{\Delta x}{2}\right) \cdot
\sin\left(x + \frac{\Delta x}{2}\right) 
\]

Тогда производная равна:

\[
f'(x) =  \lim\limits_{\Delta x \to 0} \dfrac{- 2 \cdot \sin\left(\frac{\Delta x}{2}\right) \cdot \sin\left(x + \frac{\Delta
          x}{2}\right) }{\Delta x} = - \lim\limits_{\Delta x \to 0} \left(\dfrac{\sin\left(\frac{\Delta
            x}{2}\right)}{\frac{\Delta x}{2}} \cdot \sin\left(x + \frac{\Delta x}{2}\right) \right) =
-\sin x 
\]

Получили: $\boxed{\left(\cos x\right)' = -\sin x}$


\subsection{Свойства производной}

Производные обладают следующими свойствами:

\begin{enumerate}
    \item $h(x) = C\cdot f(x)$. Тогда $h'(x) = C \cdot f'(x)$
    \item $h(x) = f(x) \pm g(x)$. Тогда: $h'(x) = f'(x) \pm g'(x)$
    \item $h(x) = f(x)\cdot g(x)$. Тогда: $h'(x) = f'(x) \cdot g(x) + f(x) \cdot g'(x)$
    \item $h(x) = \dfrac{f(x)}{g(x)}$. Тогда: $h'(x) = \dfrac{f'(x) \cdot g(x) - f(x) \cdot g'(x)}{g^2(x)}$
\end{enumerate}

Доказывать мы это не будем.

Благодаря этим свойствам можно доказать следующие утверждения:

\begin{example}
    Найти производную $h(x) = \dfrac{1}{x}$.
\end{example}


Рассмотрим $f(x) = 1$, $g(x) = x$. Тогда по свойству (4):

\[
h'(x) = \dfrac{f'(x)g'(x) - f(x)g'(x)}{g^2(x)} = \dfrac{0\cdot x - 1\cdot 1}{x^2} = -\dfrac{1}{x^2}
\]

\begin{example}
    Найти производную $f(x) = x^n$
\end{example}

Докажем методом математической индукции:

\underline{База}: $n = 1$ -- очевидно, т.к. $x' = 1\cdot x^0 = 1$

\underline{Переход}: $n = k \rightarrow n = k+1$

\[
f(x) = x^{k+1} = x^k \cdot x
\]

Тогда по свойству (3):

\[
f'(x) = \left(x^k\right)' x + x^k x' = k x^k + x^k = (k+1) x^k
\]

Получили: $\boxed{(x^n)' = n x^{n-1}, \quad n\in \mathbb{Z}}$

\begin{proposition}
    $(x^\alpha)' = \alpha x^{\alpha-1}, \quad \alpha \in \mathbb{R}$

    $\dv{e^x}{x} = e^x$    
\end{proposition}



\subsection{Таблица производных}

Ниже приведена таблица производных:
\[
\begin{aligned}
&f(x) = C \qquad&& f'(x) = 0\\
&f(x) = x^a && f'(x) = a x^{a-1}\\
%&f(x) = a^x && f'(x) = \ln a\cdot a^x\\
&f(x) = e^x && f'(x) = e^x\\
%&f(x) = \log_a x && f'(x) = \frac{1}{x \ln a},\quad (x > 0)\\
%&f(x) = \ln x && f'(x) =\frac{1}{x}\\
&f(x) = \sin x && f'(x) = \cos x \\
&f(x) =  \cos x&& f'(x) = - \sin x\\
&f(x) =  \tg x&& f'(x) = \frac{1}{\cos^2 x}\\
&f(x) =  \ctg x&& f'(x) = -\frac{1}{\sin^2 x} \\
\end{aligned}
\]



\subsection{Производная композиции}

\begin{proposition}
    Функция $f$ имеет производную в точке $x_0$ $f(x_0) = y_0$, функция $g$ имеет производную в точке
    $y_0$. Тогда $h(x)=g(f(x))$ имеет производную в точке $x_0$, и она равна $h'(x) = g'(y)\cdot f(x)$.
\end{proposition}

\begin{example}
    Найти производную функции $h(x) = \cos^2 x + 2 \cos x + 1$
\end{example}


В качестве функции $f$ и $g$ возьмем следующие функции:

\[
\left.
\begin{aligned}
f(y) &= y^2 + 2y + 1\\
g(x) &= \cos x = y
\end{aligned}
\right| \Rightarrow h'(x) = (f(g(x)))' = f'(g(x))\cdot g'(x) = \left(2\cos x + 2\right) \left(-\sin x\right)
\]

\subsection{Задачи на производные}

Смотри сет 1 на нахождение производных.

\subsection{Уравнение касательной и экстремумы}
\begin{wrapfigure}[15]{r}{5cm}
\begin{centering}
\vspace{-30pt}
\begin{tikzpicture}[domain=.6:5.2, scale=.7]
    \pgfmathsetmacro{\M}{3};
    \pgfmathsetmacro{\a}{1.2};
    \pgfmathsetmacro{\y}{-(\M-1)*(\M-7)*.6};
%Сетка
    \draw[very thin,color=gray] (-1.1,-1.1) grid (5.9,5.9);
%Оси
    \draw[->] (-1.2,0) --++ (7.4,0) node[right] {$x$};
    \draw[->] (0,-1.2) --++ (0,7.4) node[above] {$y$};
%Функция
    \draw[thick] plot[id=x] function{-(x-1)*(x-7)*.6} node[right] {$f(x)$};
%Точка
    \filldraw (\M, \y) circle(2pt);
%Касательная
    \draw[thick, blue] plot[domain = -1.3: 4] function{\a*(x - \M)-(\M-1)*(\M-7)*.6} node[above]{$y(x)$};
%Значения на осях
    \draw[thick](\M, .1) -- (\M, -.1) node[below] {$x_0$};
    \draw[thick](.1, \y) -- (-.1, \y) node[left] {$f(x_0)$};
%Угол \alpha
    \draw[thick, blue] (-.2, {\a*(-1 - \M)-(\M-1)*(\M-7)*.6}) arc(0: 50: .8) node[right] {$\alpha$};
\end{tikzpicture}
\end{centering}
\end{wrapfigure}

Мы уже говорили о \textbf{касательной}. Теперь давайте выведем уравнение касательной. 

Для этого рассмотрим функцию $f(x)$. Производная функции $f(x)$ точке $x_0$ показывает угол наклона к оси
$0x$ касательной проведенной к графику в точке $x_0$, т.е: 

\[
f'(x_0) = \tan\alpha
\]

Таким образом, можно получить уравнение касательной.

Уравнение касательной имеет вид $y(x) = ax + b$. В точке $x_0$ $y(x_0) = f(x_0)$ и $\tan \alpha = a$. 

Таким образом, уравнение касательной имеет вид:

\[
y(x) = f'(x_0)(x - x_0) + f(x_0)
\]

Теперь поговорим об \textbf{экстремумах}.

\begin{wrapfigure}[14]{r}{4cm}
\begin{centering}
\vspace{-30pt}
\begin{tikzpicture}[domain=0:2.2, scale=.9]
%Сетка
    \draw[very thin,color=gray] (-1.1,-1.1) grid (3.9,3.9);
%Оси
    \draw[->] (-1.2,0) --++ (4.4,0) node[right] {$x$};
    \draw[->] (0,-1.2) --++ (0,4.4) node[above] {$y$};
%Функция
    \draw[thick] plot[id=x] function{(x-1)*(x-1)*(x-1)+1} node[right] {$f(x)$};
    \draw[thick, blue] (-.5, 1) --++(3, 0) node[above] {$f'(1)$};
    \filldraw(1,1) circle(1.5pt);
\end{tikzpicture}
\end{centering}
\end{wrapfigure}

Часто в физике, геометрии, экономике решаются задачи на оптимизацию, т.е. на поиск оптимального решения
задачи. Обычно такие задачи сводятся к поиску экстремума (экстремум -- локальный минимум или максимум)
функции функции. 

Рассмотрим задачу, ответ к которой все знают: под каким углом к горизонту необходимо бросить тело, чтобы
дальность полета была наибольшей? 


Общая же идея в том, что если у функции в точке $x_0$ локальный экстремум, то в этой точке $f'(x_0) =
0$. \textbf{Обратно не верно}. Мысль такова, что если хотим найти экстремум чего-то, то ищем корни
уравнения на производную. 

\subsection{Задачи на нахождение экстремумов}

Смотри сет задач 2.

%\input{list2.tex}%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Интегралы}

\subsection{Криволинейная трапеция}
\begin{wrapfigure}[16]{r}{6cm}
\begin{centering}
\vspace{-30pt}
\begin{tikzpicture}[domain=-.8:5.2, scale=.8]
    \pgfmathsetmacro{\M}{1};
    \pgfmathsetmacro{\N}{4};
    \pgfmathsetmacro{\a}{.5};
%Сетка
    \draw[very thin,color=gray] (-1.1,-1.1) grid (5.9,5.9);
%Оси
    \draw[->] (-1.2,0) --++ (7.4,0) node[right] {$t\text{, с}$};
    \draw[->] (0,-1.2) --++ (0,7.4) node[above] {$v\text{, м/c}^2$};
%Функция
   \draw[thick] plot[id=f(x)] function{(x-3)*(x/6-1)*(x/6-1) - (x/6-1)*(x/6-1) - 2* (x/6-1)+3} node[right] {$v(t)$};
   \foreach\x in {0, 1, 2, 3, 4}{
        \draw ({(\x+2)/2}, 0) rectangle++(\a, {((\x+2)/2-3)*(((\x+2)/2+1)/2/6-1)*((\x+2)/2/6-1) - ((\x+2)/2/6-1)*((\x+2)/2/6-1) - 2* ((\x+2)/2/6-1)+3});
   }
   \draw({(5+2)/2}, 0) rectangle++(\a, {((7+2)/2-3)*(((7+2)/2+1)/2/6-1)*((7+2)/2/6-1) - ((7+2)/2/6-1)*((7+2)/2/6-1) - 2* ((7+2)/2/6-1)+3});
   \draw(1, .2) --++(0,-.4) node[below] {$t_1$};
   \draw(4, .2) --++(0,-.4) node[below] {$t_2$};
\end{tikzpicture}
\end{centering}
\end{wrapfigure}
Рассмотрим график зависимости скорости движение материальной точки от времени. Нас интересует расстояние,
которое тело пройдет за время $[t_1; t_2]$. 

Мысль такая: чтобы найти путь, пройденный телом необходимо посчитать площадь под графиком функции $v(t)$
на отрезке $[t_1,t_2]$. Для этого разобьем отрезок на $n$ интервалов $\Delta t_i$ и строим
прямоугольники, высотой $\min f(x)$ или $\max f(x)$ на данном промежутке. Теперь говорим что если
$n\rightarrow\infty$ а мелкость разбиения $\max\Delta t_i\rightarrow 0$ Тогда сумма площадей наших
прямоугольников стремится к площади под графиком (либо она чуть меньше, либо чуть больше в зависимости от
того $\min$ или $\max$ функции мы брали). Запишем это формулой: 

\[
S = \lim\limits_{n\rightarrow \infty} \sum\limits_{i=0}^n v(\xi) \Delta t_i
\]
где $\xi_i$ -- точка, где достигается $\min f(t)$ на интервале $\Delta t_i$.

Эта сумма называется суммой Римана.

Если вспомнить, что функция $v(t)$ является производной функции $s(t)$. То можно записать сумму Римана в
следующем виде: 

\[
S = \lim\limits_{n\rightarrow \infty} \sum\limits_{i=0}^n S'(\xi) \Delta t_i
\]

Таким образом мы выразили площадь под графиком функции $S'(t)$, через функцию $S(t)$. Запомним этот
замечательный факт и двинемся далее.

\subsubsection{Понятие первообразной, свойства}

\begin{definition}
    Функция $F(x)$~--- первообразная для функции $f(x)$, если $F'(x) = f(x)$
\end{definition}


Операция нахождения первообразной~--- обратная к дифференцированию.

Так как при дифференцировании константы <<съедаются>>, то все первообразные определены с точностью до
константы, т.е. если $F$~--- первообразная для $f$, то $F+const$ тоже первообразная для $f$ для любой
константы 

Так же можно утверждать, что если $F$~--- первообразная для $f$, то любая первообразная имеет вид $F + \const$

Теперь обсудим \textbf{свойства} первообразных:

Пусть $f$ и $g$ имеют первообразные $F$ и $G$ соответственно. Тогда:
\begin{enumerate}
    \item $f+g$ имеет первообразные, одна из которых $F+G$
    \item $k\cdot f$ имеет первообразные, одна из которых $k\cdot F$
\end{enumerate}

Это все следствия аналогичных свойств о производных.

\subsubsection{Таблица первообразных}

Так как мы находили производные для простых функций, то у нас уже имеется таблица для первообразных:

\begin{equation*}
\begin{aligned}
&f(x) = 0 \qquad && F(x) = C \\
&f(x) = x^{\alpha} && F(x) = \frac{x^{\alpha+1}}{\alpha+1} + C, \quad \alpha \neq -1\\
%&f(x) = \ln a\cdot a^x && F(x) = a^x\\
&f(x) = e^x && F(x) = e^x + C\\
%&f(x) = \log_a x && f'(x) = \frac{1}{x \ln a},\quad (x > 0)\\
%&f(x) = \ln x && f'(x) =\frac{1}{x}\\
&f(x) = \sin x && F(x) = -\cos x  + C\\
&f(x) =  \cos x&& F(x) = \sin x + C\\
&f(x) = \frac{1}{\cos^2 x} && F(x) =  \tg x + C\\
&f(x) = -\frac{1}{\sin^2 x} && F(x) =  \ctg x + C\\
\end{aligned}
\end{equation*}

\subsubsection{Неопределенный интеграл и его свойства}
Если $F$ -- первообразная для $f$, тогда семейство первообразных $F(x)+const$ называют неопределенным интегралом $f$. Запись следующая:

\[
\int f(x)\ dx = F(x) + C
\]

Наименования: $f$ -- подынтегральная функция, $f(x) \ dx$ -- подынтегральное выражение.

Из определения первообразной следует, что
\[
\dv{\int f(x) \ dx}{x} = f(x)
\]

Неопределенный интеграл <<наследует>> все свойства первообразных:

\begin{enumerate}
    \item $\int (f(x) + g(x))\ dx = \int\limits f(x) \ dx + \int\limits g(x) \ dx$
    \item $\int k\cdot f(x)\ dx = k \cdot \int\limits f(x)\ dx$
\end{enumerate}

\subsubsection{Определенный интеграл}
Вернемся к криволинейной трапеции. Мы остановились на том, что смогли выразить площадь под графиком функции $S'(t)$ через саму функцию $S(t)$:

\[
S = \lim\limits_{n\rightarrow \infty} \sum\limits_{i=0}^n S'(\xi) \Delta t_i
\]

Теперь, когда мы знаем, что такое первообразная, можно перейти к понятию определенного интеграла:

$\int\limits_a^b f(x) \ dx = \lim\limits_{n\rightarrow \infty} \sum\limits_{i=0}^n f(\xi_i) \Delta x_i $ -- определенный интеграл

$f$ называют подынтегральной функцией, $f(x) \ dx$ -- подынтегральным выражением, $a$ и $b$ -- пределы интегрирования.

Запись $\int\limits_a^b f(x) \dd{x}$ можно воспринимать как скобки. %\copyright А.И. Храбров

Так как определенный интеграл~--- сумма Римана, то геометрический смысл определенного интеграла~--- площадь под графиком

\subsection{Формула Ньютона-Лейбница}

Для того, чтобы посчитать определенный интеграл от "хороших"\ функций существует формула Ньютона-Лейбница:

\[
\int\limits_a^b f(x) \ dx = \left.F(x)\right|_a^b = F(b) - F(a)
\]

\begin{remark}
    Под <<хорошими>> функциями подразумеваются ограниченные непрерывные функции.

    Когда речь идет об определенном интеграле, все константы от первообразных сократились.

    Пределы интегрирования могут быть бесконечностью.
\end{remark}

Давайте, посчитаем наш первый интеграл


\begin{example}
    Посчитать площадь под графиком функции $f(x) = \frac{1}{x^2}$ на отрезке $[1, +\infty]$.
\end{example}


\[
\int\limits_1^\infty \frac{1}{x^2} \ dx = \left. -\frac{1}{x}\right|_1^\infty =
\left.-\frac{1}{x}\right|_\infty + \left.\frac{1}{x}\right|_1 = 1
\]

\subsection{Определенный интеграл в физике}

\begin{enumerate}
    \item $x(t) = \int\limits_0^T v(t) \ dt$~--- произвольное движение вдоль оси $x$
    \item $q(t) = \int\limits_0^T I(t) \ dt$~--- заряд, пройденный через сечение проводника за время $T$
    \item $A(t) = \int\limits_0^T N(t) \ dt$~--- работа.
    \item $\Delta P(t) = \int\limits_0^T F \ dt$~--- закон сохранения импульса
    \item Потенциал точечного заряда, если ноль потенциала выбран на бесконечности:
    \[
    \varphi(R) = -kq\int\limits_\infty^R \frac{1}{r^2}\ dr = \frac{kq}{r}
    \]
\end{enumerate}

\subsection{Простейшие свойства определенных интегралов}

Пусть $f$ и $g$ имеют первообразные $F$ и $G$ соответственно. Тогда:

\begin{enumerate}
    \item $\int\limits_a^b (f(x) + g(x))\ dx = \int\limits_a^b f(x) \ dx + \int\limits_a^b g(x) \ dx$
    \item $\int\limits_a^b k\cdot f(x)\ dx = k \cdot \int\limits_a^b f(x)\ dx$
    \item $c\in [a, b]$, тогда $\int\limits_a^b f(x)\ dx = \int\limits_a^c f(x)\ dx + \int\limits_c^b f(x)\ dx$
    \item $\int\limits_a^b f(x)\ dx = -\int\limits_b^a f(x)\ dx$
    \item $\int\limits_a^b f(Ax + B)\ dx = \frac{1}{A} \cdot \int\limits_a^b f(t)\ dt = \frac{1}{A}\cdot
        \left.F(Ax + B)\right|_a^b$~--- линейная замена переменных
\end{enumerate}

\textbf{5)}

\[
\int\limits_a^b f(t)\ dt =\left[ t = h(x) = Ax+B \right] = \int\limits_a^b \frac{f(h(x)) \ dt}{dx} dx =
\int\limits_{h(a)}^{h(b)} f(h(x)) \dv{t}{x} \ dx = \int\limits_{h(a)}^{h(b)} A\cdot f(h(x)) dx = A\cdot
\int\limits_a^b f(Ax+B) dx 
\]

\subsection{Свойства определенных интегралов, связанные со свойствами подынтегральных функций}
Очень полезные для задач, обладающих симметрией, свойства определенных интегралов. Можно рассказать
быстро, используя рисунки. 

\begin{enumerate}
    \item Если $f(x)$ -- нечетная, то $\int\limits_{-a}^a f(x)\ dx = 0$
    \item Если $f(x)$ -- четная, то $\int\limits_{-a}^a f(x)\ dx = 2\cdot\int\limits_{0}^a f(x)\ dx$
\end{enumerate}


\subsection{Задачи на определенный интеграл}

Смотри сет 3 на определенный интеграл


\end{document}