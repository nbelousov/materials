#!/usr/bin/python
# -*- coding: UTF-8 -*-

from random import normalvariate
from math import exp

Ti = normalvariate(85, 5)
Tr = normalvariate(15, 3)
alpha = 0.239
c = 443.
k = alpha/c

for i in range(30):
    t = 2 * (i + 1)
    T = Tr + (Ti - Tr) * exp(-k*t*60)
    print ("%d мин: %.0f град") % (t, T)
