from random import gauss

def h(t):
    if t <= 48:
        return abs(gauss(0.1868 * t, 0.2))
    if t <= 95:
        return gauss(9 + 0.1273 * (t - 48), 0.2)
    return gauss(15, 0.2)

while True:
    try:
        t = int(round(float(input())))
    except (TypeError, ValueError):
        print("NaN")
        continue
    except KeyboardInterrupt:
        print('\nGoodbye')
        exit()
    if t <= 0:
        print('There is no liquid water in the bucket')
    elif t >= 300:
        print('The experiment has ended')
        exit()
    else:
        print(round(h(t), 1), "cm")

