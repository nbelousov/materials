\section{Тема 3}

Рассмотрим вращение твердого тела с неподвижной точкой. В каждый момент времени существует вектор угловой
скорости $\mathbf{\omega}$. Тогда каждая точка обладает скоростью $\left[\mathbf{\omega}, \mathbf{r}
\right]$. Момент импульса тогда запишется в виде
\begin{equation}
	\mathbf{L} = \sum m \left[ \mathbf{r}, \left[ \mathbf{\omega}, \mathbf{r} \right] \right].
\end{equation}

Научимся раскрывать двойное векторное произведение. Проверим формулу <<$BAC-CAB$>> на базисных векторах.
\[
	\mathbf{L} = \sum m \left( \mathbf{\omega} r^2 - \mathbf{r} \big( \mathbf{r}, \mathbf{\omega} \big)
    \right).
\]

В кординатах это выражение примет вид
\[
	L_i = \sum m \left( \omega_i r^2 - r_i \sum\limits_{j = 1}^3 r_j \omega_j \right),
\]

\begin{equation}
	L_i = \sum m \sum\limits_{j = 1}^3 \left( \delta_{i j} r^2 - r_i r_j \right) \omega_j =
    \sum\limits_{j = 1}^3 I_{i j} \omega_j.
\end{equation}

$I_{ij}$ набор числовых коэффициентов, задающий правило пересчета $\mathbf{\omega}$ в $\mathbf{L}$,
который в общем случае отличается от умножения на число. Так как этот набор имеет определенные правила
преобразования при замене системы координат, он назывется тензором инерции
\begin{equation}
	I_{ij} = \sum m \left(  \delta_{i j} r^2 - r_i r_j \right)
\end{equation}

Из него можно получить момент инерции относительно оси
\begin{equation}
\label{I}
	I = \sum\limits_{i = 1}^3 \sum\limits_{j = 1}^3 I_{ij} n_i n_j.
\end{equation}

Можно найти энергию вращающегося тела
\[
	E = \sum \frac{1}{2} m v^2 = \sum \frac{1}{2} m \left[\mathbf{\omega}, \mathbf{r} \right]^2.
\]

Воспользуемся (и докажем) циклической перестановкой в смешанном произведении
\begin{equation}
	E = \sum \frac{1}{2} m \big( \left[\mathbf{r}, \left[\mathbf{\omega}, \mathbf{r} \right] \right],
    \mathbf{\omega}\big) = \frac{1}{2} \big( \mathbf{L}, \mathbf{\omega} \big).
\end{equation}

\begin{equation}
	E = \frac{1}{2} \sum\limits_{i = 1}^3 \sum\limits_{j = 1}^3 I_{ij} \omega_i \omega_j.
\end{equation}

В симметричном случае $\mathbf{L} \sim \mathbf{\omega}$
\[
	E = \frac{1}{2} I \omega^2.
\]

Из формулы (\ref{I}) можно получить полезную конструкцию
\begin{equation}
	\mathbf{{\rho}} = \frac{\mathbf{n}}{\sqrt{I}}.
\end{equation}

Тогда $\rho$ удовлетворяет следующему равенству
\begin{equation}
	1 = \sum \limits_{i = 1}^3 \sum \limits_{j = 1}^3 I_{ij} \rho_i \rho_j.
\end{equation}
Множество всех подходящих векторов $\mathbf{\rho}$ определяет в пространстве эллипсоид (без док-ва). Так как при 
свободном вращении сохраняется энергия и момент импульса, данную конструкцию можно использовать для 
наглядного понимания того, как будет вращаться тело в различных условиях.

А именно, для твердого тела $\mathbf{\rho}$ должен лежать на поверхности эллипсойда. Его направление совпадает с 
осью вращения. Нормаль в точке $\mathbf{\rho}$ находится следующим образом
\begin{equation}
	\pdv{\rho_k} \sum\limits_{i = 1}^3 \sum\limits_{j = 1}^3 I_{ij} \rho_i \rho_j = \sum\limits_{i = 1}^3
    \sum\limits_{j = 1}^3 I_{ij} \left( \delta_{ik} \rho_j + \rho_i \delta_{jk} \right) =
    \sum\limits_{i = 1}^3 I_{ik} \rho_i = \sum\limits_{i = 1}^3 I_{ik} \frac{\omega_i}{\sqrt{2E}} =
    \frac{L_k}{\sqrt{2E}}, 
\end{equation}
а значит сонаправлена с моментом импульса. Кроме того, проекция $\mathbf{\rho}$ на эту нормаль $\mathbf{n}$
\begin{equation}
	\left( \mathbf{\rho}, \mathbf{n} \right) = \frac{1}{\sqrt{2E}} \frac{1}{L} \left( \mathbf{\omega},
      \mathbf{L} \right) =  \frac{\sqrt{2E}}{L} = \mathrm{const}.
\end{equation}
И тогда при вращении твердого тела его эллипсойд инерции будет <<катиться>> плоскости, сохраняя центр на 
постоянной высоте. Характер движения будет сильно зависить от энергии и момента импульса, а также от формы 
самого эллипсойда.

\begin{figure}[h!]
	\centering
	\input{pics/rot-ellipse.tex}
	\caption{Эллипсойд инерции, и где на нем что.}
\end{figure}

Оси этого эллипсойда $Ox', Oy' ,Oz'$ можно назвать главными осями инерции. Вращения вокруг одних осей будут 
устойчивыми, вокруг других могут быть неустойчивыми. В общем случае все три оси разные. Если две из них 
совпадают по величине, тело называют симметричным волчком или ротатором, если все три, то~--- шаровым 
волчком.

Удобной оказывается вращающаяся система, жестко связанная с телом, оси которой совпадают с главными осями и 
проходят через центр масс. В ней тензор инерции является постоянным и имеет простой вид
\begin{align}
	I_{ij} &= 0, \ \ \ \text{если} \ \ i \neq j, \\
	I_{ij} &= I_{i}, \ \ \ \text{если} \ \ i = j.
\end{align}

Это можно удобно записать в виде матрицы
\begin{equation}
	I_{ij} = \left(
	\begin{matrix}
		I_{11} & I_{12} & I_{13} \\
		I_{21} & I_{22} & I_{23} \\
		I_{31} & I_{32} & I_{33} 
	\end{matrix}
	\right)
\end{equation}
в общем виде, и 
\begin{equation}
	I_{ij} = \left(
	\begin{matrix}
		I_1 & 0 & 0 \\
		0 & I_2 & 0 \\
		0 & 0 & I_3 
	\end{matrix}
	\right)
\end{equation}
в связанной с телом системе отсчета.

Рассмотрим в этой системе отсчета уравнение (\ref{dLdt}). Правую часть нужно просто разложить по базису этой 
системы, а левую можно немного преобразовать. Согласно формуле (\ref{arb_vec_rot})
\begin{equation}
	\dv{t} \mathbf{L} = \dv{t} \mathbf{L'} + \left[\mathbf{\omega}, \mathbf{L} \right].
\end{equation}
Запись $\dv{t}\mathbf{L'}$ нужно воспринимать следующим образом. Это часть изменения вектора $\mathbf{L}$, 
соответствующая его изменению относительно вращающейся системы отсчета. Тогда уравнение (\ref{dLdt}) примет 
следующий вид
\begin{equation}
	\dv{t} \mathbf{L'} + \left[\mathbf{\omega}, \mathbf{L} \right] = \sum\limits_{i = 1}^{n}
    \sum\limits_{k = 1}^{N_i} \mathbf{M}_{ik}.
\end{equation}

Если теперь расписать его по компонентам выбранной системы
\begin{align}
	&I_1 \dot{\omega}_1 + \left( I_3 - I_2 \right) \omega_3 \omega_2 = M_1, \\	
	&I_2 \dot{\omega}_2 + \left( I_1 - I_3 \right) \omega_1 \omega_3 = M_2, \\
	&I_3 \dot{\omega}_3 + \left( I_2 - I_1 \right) \omega_2 \omega_1 = M_3.
\end{align}

Свободные вращения вокруг главных осей стационарны. Например $\omega_1 \neq 0$, а $\omega_2 = \omega_3 =
0$
\begin{equation}
	I_1 \dot{\omega}_1 = 0 
\end{equation}
\[
	\omega_1 = const.
\]
Выбор оси может быть произвольным.

Покажем что одни вращения будут устойчивыми, а другие нет. Пусть $I_1 > I_2 > I_3$, и вращение происходит с 
вкомпонентами угловой скорости $|\omega_1| \gg |\omega_2|$ и $|\omega_1| \gg |\omega_3|$. Тогда
\begin{equation}
	\dot{\omega}_1 = \frac{I_2 - I_3}{I_1} \omega_3 \omega_2 \approx 0
\end{equation}
Тогда 
\[
	\omega_1 = const.
\]
Остаются два уравнения
\begin{align}
	& \dot{\omega}_2 = \frac{I_3 - I_1}{I_2} \omega_1 \omega_3, \\
	& \dot{\omega}_3 = \frac{I_1 - I_2}{I_3} \omega_2 \omega_1.
\end{align}
Возьмем производную от второго уравнения и подставим $\dot{\omega}_2$ из первого. Получится уравнение
\begin{equation}
	\ddot{\omega}_3 = \left( \frac{I_1 - I_2}{I_3} \right) \omega_1^2 \left( \frac{I_3 - I_1}{I_2} \right) \omega_3
\end{equation}
С учетом соотношения главных моментов инерций знак коэффициента $\Omega_1 = \left( \frac{I_1 - I_2}{I_3} 
\right) \omega_1^2 \left( \frac{I_3 - I_1}{I_2} \right)$ оказывается отрицательным, и решением является колебания 
$\omega_3$
\begin{equation}
	\omega_3 = A \cos{\left( \sqrt{|\Omega_1|} t \right)} + B \sin{\left( \sqrt{|\Omega_1|} t \right)}.
\end{equation}
Нетрудно убедиться, что $\omega_2$ тоже будет совершать колебания, следовательно и $\omega_3$ и $\omega_2$ 
будут оставаться малыми впроцессе движения.

Аналогичная ситуация получится при вращении вокруг $I_3$. Рассмотрим вращение вокруг $I_2$. Для него
система уравнений придет к следующему виду
\begin{align}
	& \omega_2 = const, \\
	& \dot{\omega}_1 = \frac{I_2 - I_3}{I_1} \omega_3 \omega_2, \\
	& \dot{\omega}_3 = \frac{I_1 - I_2}{I_3} \omega_2 \omega_1.
\end{align}
Аналогичными действиями сведем ее к одному уравнению
\begin{equation}
	\ddot{\omega}_3 = \left( \frac{I_1 - I_2}{I_3} \right) \omega_2^2 \left( \frac{I_2 - I_3}{I_1}
    \right) \omega_3.
\end{equation}
В отличие от предыдущих случаев, коэффициент $\Omega_1 = \left( \frac{I_1 - I_2}{I_3} \right) \omega_2^2
\left( \frac{I_2 - I_3}{I_1} \right)$ оказывается положительным. Это значит, что решение будет иметь вид
\begin{equation}
	\omega_3 = A e^{\sqrt{|\Omega_2|}t} + B e^{-\sqrt{|\Omega_2|}t}
\end{equation}
После выражения $\omega_1$
\begin{equation}
	\omega_1 = \widetilde{A} e^{\sqrt{|\Omega_2|}t} + \widetilde{B} e^{-\sqrt{|\Omega_2|}t}
\end{equation}
Если хоть в какой-то момент $\omega_1$ или $\omega_3$ увеличивается, то $A$ или $\widetilde{A}$ отличны
от нуля, и рано или поздно $\omega_1$ или $\omega_3$ станут не пренебрежимо малы. Поэтому такое вращение
неустойчиво.
